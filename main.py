import csv
import math
from functools import reduce

import matplotlib.pyplot as plt


def main():
    f_coordinates = []
    omega_coordinates = []
    pi_coordinates = []
    dicts_by_letter = {'Ф (Ф)': f_coordinates, 'Омега (Ω)': omega_coordinates, 'Пи (π)': pi_coordinates}
    with open('./assets/test_public_probes_intersetions.csv') as table:
        reader = csv.DictReader(table, fieldnames=['1', '2', '3', '4'])
        actual_dict = {}
        for i, row in enumerate(reader):
            new_dict = dicts_by_letter.get(row.get('1'))
            if new_dict is not None:
                actual_dict = new_dict
            actual_dict.append({'x': int(row['2']), 'y': int(row['3'])})

    f_x = [obj['x'] for obj in f_coordinates]
    f_y = [obj['y'] for obj in f_coordinates]
    omega_x = [obj['x'] for obj in omega_coordinates]
    omega_y = [obj['y'] for obj in omega_coordinates]
    pi_x = [obj['x'] for obj in pi_coordinates]
    pi_y = [obj['y'] for obj in pi_coordinates]

    plt.scatter(f_x, f_y,
                c='blue', label='Ф', marker='>')
    plt.scatter(omega_x, omega_y,
                c='green', label='Ω', marker='^')
    plt.scatter(pi_x, pi_y,
                c='red', label='π', marker='<')

    f_center = class_center(f_x, f_y)
    omega_center = class_center(omega_x, omega_y)
    pi_center = class_center(pi_x, pi_y)
    plt.scatter(f_center.get('x'), f_center.get('y'),
                c='blue', label='Эталон класса Ф', marker='.')
    plt.scatter(omega_center.get('x'), omega_center.get('y'),
                c='green', label='Эталон класса Ω', marker='.')
    plt.scatter(pi_center.get('x'), pi_center.get('y'),
                c='red', label='Эталон класса π', marker='.')

    plt.xlabel('Пересечения с горизонтальным зондом')
    plt.ylabel('Пересечения с вертикальным зондом')
    plt.title('Визуализация объектов в пространстве признаков')
    plt.legend()
    plt.savefig('plot.png')

    # pragma classification
    x_coord = float(input('Введите координату x новой точки:'))
    y_coord = float(input('Введите координату y новой точки:'))

    distance_to_f_center = euclide_distance(x_coord, y_coord, f_center.get('x'), f_center.get('y'))
    distance_to_omega_center = euclide_distance(x_coord, y_coord, omega_center.get('x'), omega_center.get('y'))
    distance_to_pi_center = euclide_distance(x_coord, y_coord, pi_center.get('x'), pi_center.get('y'))

    if distance_to_pi_center < distance_to_f_center and distance_to_pi_center < distance_to_omega_center:
        plt.scatter(x_coord, y_coord,
                    c='red', marker=',')
    elif distance_to_omega_center < distance_to_f_center and distance_to_omega_center < distance_to_pi_center:
        plt.scatter(x_coord, y_coord,
                    c='green', marker=',')
    elif distance_to_f_center < distance_to_pi_center and distance_to_f_center < distance_to_omega_center:
        plt.scatter(x_coord, y_coord,
                    c='green', marker=',')
    else:
        print('two or more distances are equal')

    plt.savefig('new_plot.png')
    print(f'расстояние до центра Ф - {distance_to_f_center}\nрасстояние до центра Ω - {distance_to_omega_center}\n'
          f'расстояние до центра π - {distance_to_pi_center} ')

def min(values):
    ret = values[0]
    for val in values:
        if val < min:
            ret = val
    return ret


def euclide_distance(x_1, y_1, x_2, y_2):
    return math.sqrt((x_2 - x_1) ** 2 + (y_2 - y_1) ** 2)


def class_center(x_list, y_list):
    avg_x = reduce(lambda acc, cur: acc + cur, x_list) / len(x_list)
    avg_y = reduce(lambda acc, cur: acc + cur, y_list) / len(y_list)
    return {'x': avg_x, 'y': avg_y}


if __name__ == '__main__':
    main()
